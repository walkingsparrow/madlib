#include <dbconnector/dbconnector.hpp>

#include "crossprod.hpp"

namespace madlib {
namespace modules {
namespace linalg {

using namespace dbal::eigen_integration;
using madlib::dbconnector::postgres::madlib_get_typlenbyvalalign;
using madlib::dbconnector::postgres::madlib_construct_array;

// ----------------------------------------------------------------------

typedef struct __type_info{
    Oid oid;
    int16_t len;
    bool    byval;
    char    align;
    
    __type_info(Oid oid):oid(oid) {
        madlib_get_typlenbyvalalign(oid, &len, &byval, &align);
    }
} type_info;

static type_info FLOAT8TI(FLOAT8OID);

// ----------------------------------------------------------------------

AnyType __pivotalr_crossprod_transition::run (AnyType& args)
{
    ArrayHandle<double> left = args[1].getAs<ArrayHandle<double> >();
    ArrayHandle<double> right = args[2].getAs<ArrayHandle<double> >();
    int m = left.size(), n = right.size();
    MutableArrayHandle<double> state(NULL);

    if (args[0].isNull()) {
        state = madlib_construct_array(NULL, m*n, FLOAT8TI.oid, FLOAT8TI.len,
                                       FLOAT8TI.byval, FLOAT8TI.align);
        for (size_t i = 0; i < state.size(); i++) state[i] = 0;
    } else
        state = args[0].getAs<MutableArrayHandle<double> >();

    int count = 0;
    for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
            state[count++] += left[i] * right[j];

    return state;
}

// ----------------------------------------------------------------------

AnyType __pivotalr_crossprod_merge::run (AnyType& args)
{
    if (args[0].isNull() && args[1].isNull()) return args[0];
    if (args[0].isNull()) return args[1].getAs<ArrayHandle<double> >();
    if (args[1].isNull()) return args[0].getAs<ArrayHandle<double> >();

    MutableArrayHandle<double> state1 = args[0].getAs<MutableArrayHandle<double> >();
    ArrayHandle<double> state2 = args[1].getAs<ArrayHandle<double> >();
    for (size_t i = 0; i < state1.size(); i++) state1[i] += state2[i];
    return state1;
}

// ----------------------------------------------------------------------

AnyType __pivotalr_crossprod_sym_transition::run (AnyType& args)
{
    ArrayHandle<double> arr = args[1].getAs<ArrayHandle<double> >();
    int n = arr.size();
    MutableArrayHandle<double> state(NULL);

    if (args[0].isNull()) {
        state = madlib_construct_array(NULL, n*(n+1)/2, FLOAT8TI.oid, FLOAT8TI.len,
                                       FLOAT8TI.byval, FLOAT8TI.align);
        for (size_t i = 0; i < state.size(); i++) state[i] = 0;
    } else
        state = args[0].getAs<MutableArrayHandle<double> >();

    int count = 0;
    for (int i = 0; i < n; i++)
        for (int j = 0; j <= i; j++)
            state[count++] += arr[i] * arr[j];

    return state;
}

}
}
}
